# -*- coding: utf-8 -*-

from os import listdir, path, remove

from kivy.app import App
from kivy.core.audio import SoundLoader
from kivy.lang import Builder
from kivy.uix.boxlayout import BoxLayout
from kivy.uix.button import Button
from kivy.uix.filechooser import FileChooserListView

Builder.load_string(
    """
<KabacaPlayer>:
    id: kabaca_player
    FileChooserListView:
        id: filechooser
    Button:
        text: 'Tocar'
        on_release: kabaca_player.open(filechooser.path)
    Button:
        text: 'Lixo'
        on_release: kabaca_player.lixo()
"""
)


class KabacaPlayer(BoxLayout):
    def listar_arquivos_mp3(self, caminho):
        arquivos = []
        for f in [f for f in listdir(caminho)]:
            if path.isfile(path.join(caminho, f)) and (path.join(caminho, f)).endswith(
                ".mp3"
            ):
                arquivos.append(path.join(caminho, f))
            elif path.isdir(path.join(caminho, f)):
                arquivos = arquivos + self.listar_arquivos_mp3(path.join(caminho, f))
        return arquivos

    def open(self, caminho):
        self.arquivos = self.listar_arquivos_mp3(caminho)
        self.aux = 0
        self.sound = SoundLoader.load(self.arquivos[self.aux])
        if self.sound:
            self.sound.bind(on_stop=self.stop_event)
            self.sound.play()

    def stop_event(self, sound):
        self.sound.unload()
        self.aux = self.aux + 1
        self.sound = SoundLoader.load(self.arquivos[self.aux])
        if self.sound:
            self.sound.bind(on_stop=self.stop_event)
            self.sound.play()

    def lixo(self):
        remove(self.sound.source)
        self.sound.stop()


class MyApp(App):
    def build(self):
        return KabacaPlayer()


if __name__ == "__main__":
    MyApp().run()
