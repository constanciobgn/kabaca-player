import unittest

from main import KabacaPlayer


class KabacaPlayerTest(unittest.TestCase):
    def setUp(self):
        self.caminho = "musicas_testes"
        self.kabaca_player = KabacaPlayer()

    def test_quantidade_de_arquivos_mp3(self):
        self.assertEqual(3, len(self.kabaca_player.listar_arquivos_mp3(self.caminho)))

    def test_nome_arquivos_mp3(self):
        arquivos = self.kabaca_player.listar_arquivos_mp3(self.caminho)
        self.assertIn("musicas_testes/03 Vaqueiro Milionário[1].mp3", arquivos)


if __name__ == "__main__":
    unittest.main()
