# coding: utf-8

# Usando @pacth para substituir um método original

from mock import patch
import os
import unittest

def fake_remove(path, *a, **k):
    print('remove done')

@patch('os.remove', fake_remove)
def test():
    try:
        os.remove('qualquer coisa')
    except OSError as e:
        print e
    else:
        print 'test success'



# Usando @pacth para substituir um método original por um mock que sempre retorna um valor pre-definido

class Calculator:

    def sum(self, a, b):
        return a + b


class TestCalcular(unittest.TestCase):
    @patch(Calculator.sum, return_value=9)
    def test_sum(self, sum):
        self.assertEqual(sum(2,3), 9)






if __name__ == '__main__':
    test()
    unittest.main()
